import Axios from 'axios';

function getAUthHeaders() { 
  return  {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
  };
} 

function publicPost(url, payload, headers = null) {
  try {
    return Axios.post(url, payload, headers);
  } catch (error) {
    console.log(error);
    throw error;
  }
}

function post(url, payload) {
  try {
    return Axios.post(url, payload, { headers: getAUthHeaders() });
  } catch (error) {
    console.log(error);
    throw error;
  }
}

function get(url) {
  try {
    return Axios.get(url, { headers: getAUthHeaders() });
  } catch (error) {
    console.log(error);
    throw error;
  }
}

function put(url, payload) {
  try {
    return Axios.put(url, payload, { headers: getAUthHeaders() });
  } catch (error) {
    console.log(error);
    throw error;
  }
}

function deleteById(url) {
  try {
    return Axios.delete(url, { headers: getAUthHeaders() });
  } catch (error) {
    console.log(error);
    throw error;
  }
}

export default {
  publicPost,
  post,
  get,
  put,
  deleteById,
};
