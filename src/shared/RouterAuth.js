import _ from 'lodash';

function hasLoggedIn() {
    const token = localStorage.getItem('token');
    return !_.isUndefined(token) && !_.isNull(token) && !_.isEmpty(token) 
}

export default {
    hasLoggedIn
}