import React from 'react';
import { Modal, Button } from 'react-bootstrap';

export default function AlertComponent(props) {
  const {
    title = 'Are you sure?',
    buttonconfirm = {
      showConfirm: false,
      confirmText: 'OK',
    },
    buttoncancel = {
      showCancel: false,
      cancelText: 'Cancel'
    },
    text = '',

  } = props;
  return (
    <Modal
      {...props}
      size='lg'
      aria-labelledby='contained-modal-title-vcenter'
      centered
      backdrop={true}
    >
      <Modal.Body className='alert-model'>
        <h4>{title}</h4>
        <p>
          {text}
        </p>  
        {buttonconfirm.showConfirm ? <Button  onClick={(e)=>{props.onConfirm(true)}}>{buttonconfirm.confirmText}</Button> : null}
        {buttoncancel.showCancel ? <Button  onClick={(e) =>{props.onHide(true)}}>{buttoncancel.cancelText}</Button> : null}
      </Modal.Body>
    </Modal>
  );
}
