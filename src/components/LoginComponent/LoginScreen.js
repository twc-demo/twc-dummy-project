import React, {Component} from 'react';
import './Login.scss';
import { Row, Col, Form, Button } from 'react-bootstrap';
import logo from '../../assets/images/logo-new/logo.png';
import serviceLayerClient from '../../shared/ServiceLayerClient';
import { Redirect } from "react-router-dom";
import Config from '../../shared/Config';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      redirect: null
    }
  }

  async login(e) {
    e.preventDefault()
    const { email, password } = this.state;
    const {status, data} = await serviceLayerClient.publicPost(`${Config.BaseUrl}/twc/login`, { email, password });
    if (status === 200) {
      localStorage.setItem('token', data);
      this.setState({ redirect: "/application/welcome" });
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} push />
    }

    return (
      <Row className='login-wrapper'>
      <Col xs={12} md={6} className='login-left'>
        <div className='login-form-wrap'>
          <div className='greeting'>
            <h1>Hi there, </h1>
            <p>
              Welcome to our <br></br> test product
            </p>
          </div>

          <Form>
            <Form.Group controlId='formBasicEmail'>
              <Form.Control
                className='login-input'
                type='email'
                  placeholder='e-mail'
                  value={this.state.email}
                  onChange={(e)=>this.setState({email:e.target.value})}
              />
            </Form.Group>
            <Form.Group controlId='formBasicPassword'>
              <Form.Control
                  className='login-input'
                  type='password'
                  placeholder='password'
                  value={this.state.password}
                  onChange={(e) => this.setState({ password: e.target.value })}
              />
            </Form.Group>

            <Button onClick={(e)=>this.login(e)} variant='primary' type='submit' className='btn-login'>
              login
            </Button>
          </Form>
        </div>
      </Col>
      <Col xs={12} md={6} className='login-right'>
        <div className='context-logo'>
          <img src={logo}></img>
          <p>
            test <br></br> project
          </p>
        </div>
      </Col>
    </Row>
    )
  }
}

