import serviceLayerClient from '../../../../../shared/ServiceLayerClient';
import Config from '../../../../../shared/Config';


function createDummy(payload) {
  const url = `${Config.BaseUrl}/twc/dummy/create`;
  return serviceLayerClient.post(url, payload);
}

function getDummies() {
  const url = `${Config.BaseUrl}/twc/dummy/all`;
  return serviceLayerClient.get(url);
}

function updateDummy(payload) {
  const url = `${Config.BaseUrl}/twc/dummy/update`;
  return serviceLayerClient.put(url, payload);
}

function deleteDummyById(id) {
  const url =`${Config.BaseUrl}/twc/dummy/${id}`;
  return serviceLayerClient.deleteById(url);
}

export default {
  createDummy,
  getDummies,
  updateDummy,
  deleteDummyById,
};
