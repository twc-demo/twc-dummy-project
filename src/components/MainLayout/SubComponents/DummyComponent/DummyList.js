import React, { Component } from 'react';
import './Dummy.scss';
import { Table, Col, Button } from 'react-bootstrap';
import Dummy from './Dummy';
import DummyService from './Services/DummyService';
import { Redirect } from 'react-router-dom';

export default class DummyList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dummyList: [],
      redirect: null,
    };
  }
  async loadDummies() {
    const { status, data } = await DummyService.getDummies();
    console.log(data);
    if (status === 200) {
      this.setState({
        dummyList: data,
      });
    }
  }

  reload(value) {
    if (value) {
      this.loadDummies();
    }
  }

  componentDidMount() {
    this.loadDummies();
  }
  addDummy() {
    this.setState({
      redirect: '/application/create',
    });
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} push />;
    }
    return (
      <div>
        <div className='dl-top-banner'>
          <h1>Dummies</h1>
          <Button onClick={() => this.addDummy()} className='btn-m dummy-btn'>
            Add New Dummy
          </Button>
        </div>
        <Col className='dummy-list-container'>
          <Table borderless={true} responsive>
            <thead>
              <tr>
                <th className='image-col'></th>
                <th>Dummy name</th>
                <th>email</th>
                <th>phone number</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.state.dummyList.map((dummy) => {
                return (
                  <Dummy
                    loadDummies={(e) => this.reload(e)}
                    key={dummy._id}
                    dummyData={dummy}
                  />
                );
              })}
            </tbody>
          </Table>
        </Col>
      </div>
    );
  }
}
