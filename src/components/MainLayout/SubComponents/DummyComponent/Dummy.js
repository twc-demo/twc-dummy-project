import React, { Component } from 'react';
import { Row, Col, Button, Image, Form } from 'react-bootstrap';
import trash from '../../../../assets/images/trash.svg';
import pencil from '../../../../assets/images/pencil.svg';
import DummyService from './Services/DummyService';
import AlertComponent from '../../../AlertComponent/AlertComponent';

export default class Dummy extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    const {
      editable = false,
      edited = false,
      name,
      email,
      image,
      phone,
      _id,
    } = props.dummyData;
    this.state = {
      editable,
      edited,
      name,
      email,
      image,
      phone,
      _id,
      showAlert: false,
      alertText: '',
      alertAction: null,
      showCancel: false,
      confirmText: 'Yes',
      showConfirm: true,
    };
  }
  edit() {
    this.setState({ editable: true });
    this.setState({ edited: true });
  }
  async save() {
    const { name, email, phone, _id } = this.state;
    const { status } = await DummyService.updateDummy({
      name,
      email,
      phone,
      _id,
    });
    if (status === 200) {
      this.setState({
        showCancel: false,
        alertText: 'Your Dummy Has Successfully Been Saved',
        showAlert: true,
        confirmText: 'OK',
        alertAction: 'edit-success',
      });
    }
    this.props.loadDummies();
    this.setState({ editable: false });
    this.setState({ edited: false });
  }
  async deleteDummy() {
    const { status } = await DummyService.deleteDummyById(this.state._id);
    if (status === 200) {
      this.setState({
        showCancel: false,
        alertText: 'Your Dummy Has Successfully Been Deleted',
        showAlert: true,
        alertAction: 'delete-success',
        confirmText:'OKay'
      });
    }
  }

  deleteAlert() {
    this.setState({
      showCancel: true,
      alertText: `Do You Want To Delete The Dummy '${this.state.name}'?`,
      showAlert: true,
      alertAction: 'delete',
    });
  }
  onConfirm(e) {
    if (e) {
      switch (this.state.alertAction) {
        case 'delete':
          this.deleteDummy();
          break;
        case 'delete-success':
          this.props.loadDummies(true);
          break;
        case 'edit-success':
          this.setState({
            showCancel: false,
            alertText: '',
            showAlert: false,
            alertAction: null,
          });
          this.props.loadDummies(true);
          break;
        default:
          break;
      }
    }
  }
  render() {
    return (
      <>
        <tr className='dummy-container'>
          <td className='dummy-image'>
            <Image src={this.state.image}></Image>
          </td>
          <td className='text-fields'>
            <Form.Control
              type='text'
              placeholder='name'
              disabled={!this.state.editable}
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}
            />
          </td>
          <td className='text-fields'>
            <Form.Control
              type='text'
              placeholder='email'
              disabled={!this.state.editable}
              value={this.state.email}
              onChange={(e) => this.setState({ email: e.target.value })}
            />
          </td>
          <td className='text-fields'>
            <Form.Control
              type='text'
              placeholder='phone'
              disabled={!this.state.editable}
              value={this.state.phone}
              onChange={(e) => this.setState({ phone: e.target.value })}
            />
          </td>
          <td className='action-btn-area'>
            {this.state.edited ? (
              <Button onClick={(e) => this.save()} className='btn-m'>
                Save
              </Button>
            ) : (
              <>
                <Image
                  className='img-btn'
                  onClick={() => {
                    this.edit();
                  }}
                  src={pencil}
                ></Image>
                <Image
                  onClick={(e) => this.deleteAlert()}
                  className='img-btn'
                  src={trash}
                ></Image>
              </>
            )}
          </td>
        </tr>
        <AlertComponent
          show={this.state.showAlert}
          buttonconfirm={{
            showConfirm: this.state.showConfirm,
            confirmText: this.state.confirmText,
          }}
          buttoncancel={{
            showCancel: this.state.showCancel,
            cancelText: 'Cancel',
          }}
          onHide={(e) => this.setState({ showAlert: false })}
          onConfirm={(e) => this.onConfirm(e)}
          title={this.state.alertText}
        />
      </>
    );
  }
}
