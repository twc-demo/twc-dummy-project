import React, { Component } from 'react';
import { Col, Form, Button } from 'react-bootstrap';
import './Dummy.scss';
import AlertComponent from '../../../AlertComponent/AlertComponent';
import dummyService from './Services/DummyService';
import _ from 'lodash';
import { Redirect } from 'react-router-dom';


export default class AddDummy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      image: { hasImage: false, imageData: '' },
      showAlert: false,
      formError: '',
      confirmText: '',
      redirect:null
    };
  }
  componentDidMount() {
  console.log(12324);
}
  onConfirm(event) {
    if (event) {
         this.setState({
       showAlert: false,
      redirect: '/application/list'
    });
    }
 
  }
  async addDummy() {
    const {
      image: { hasImage,imageData },
      name,
      phone,
      email,
    } = this.state;
    if (
      _.isEmpty(name) ||
      _.isEmpty(phone) ||
      _.isEmpty(email) ||
      !hasImage
    ) {
      this.setState({ formError: 'please fill all form fields to proceed!!' });
    } else {
      this.setState({ formError: '' });
      const fd = new FormData();
      fd.append('name', name);
      fd.append('phone', phone);
      fd.append('email', email);
      fd.append('file', imageData);
      const {status} = await dummyService.createDummy(fd);
     if (status === 200) {
       this.setState({      
         confirmText: 'Your Dummy Has Successfully Been Saved',
         showAlert: true,
       })
     }
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect}  push/>
    }
    return (
      <div>
        <Form>
          <Form.Group controlId='formGroupName'>
            <Form.Label>full name</Form.Label>
            <Form.Control
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}
              type='text'
              placeholder=''
            />
          </Form.Group>
          <Form.Group controlId='formGroupEmail'>
            <Form.Label>e-mail</Form.Label>
            <Form.Control
              value={this.state.email}
              onChange={(e) => this.setState({ email: e.target.value })}
              type='email'
              placeholder=''
            />
          </Form.Group>
          <Form.Group controlId='formGroupPhone'>
            <Form.Label>phone number</Form.Label>
            <Form.Control
              value={this.state.phone}
              onChange={(e) => this.setState({ phone: e.target.value })}
              type='number'
              placeholder=''
            />
          </Form.Group>
          <Form.Group controlId='formGroupImage'>
            <Form.Label>profile picture</Form.Label>
            <Form.Control
              onChange={(e) =>
                this.setState({
                  image: { hasImage: true, imageData: e.target.files[0] },
                })
              }
              className='file-input'
              type='file'
              accept='image/png, image/jpeg'
            />
          </Form.Group>
          <Button className='btn-m btn-dummy' onClick={() => this.addDummy()}>
            Add New Dummy
          </Button>
          {!_.isEmpty(this.state.formError) ? (
            <span>{this.state.formError}</span>
          ) : null}
        </Form>
        <AlertComponent
        show={this.state.showAlert}
        buttonconfirm={{ showConfirm: true, confirmText:'OKay' }}
        onHide={(e) => this.setState({showAlert:false})}
          onConfirm={(e)=>this.onConfirm(e)}
          title={this.state.confirmText}
      />
      </div>
    );
  }
}
