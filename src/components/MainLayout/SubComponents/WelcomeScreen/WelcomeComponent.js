import React, { useState } from 'react';
import './WelcomeScreen.scss';
import { Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';


export default function WelcomeComponent() {
  const [redirect, setRedirect] = useState(null);
  if (redirect) {
    return <Redirect to={redirect} push />;
  }
  return (
    <div className='welcome-container'>
      <h1>Welcome!</h1>
      <p>
        This is where your dummy users will live. Click the button below to add
        a new dummy.
      </p>
      <Button onClick={()=>{setRedirect('./create')}} className='btn-m btn-dummy'>Add Your First Dummy</Button>
    </div>
  );
}
