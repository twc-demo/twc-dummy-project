import React from 'react';
import './MainLayout.scss';
import { Row, Col } from 'react-bootstrap';
import WelcomeComponent from './SubComponents/WelcomeScreen/WelcomeComponent';
import AddDummy from './SubComponents/DummyComponent/AddDummy';
import DummyList from './SubComponents/DummyComponent/DummyList';
import { BrowserRouter as Router, Switch, Route,Redirect } from 'react-router-dom';
import routerAuth from '../../shared/RouterAuth';

export default function MainLayoutComponent() {
  if (!routerAuth.hasLoggedIn()) {
    return <Redirect to="/login" push/>
  }
  return (
    <Row className='ml-container'>
      <Col fluid="true" className='ml-mid'>
        <div className='ml-context'>
          <p className='stamp'>
            test <br></br> project
          </p>
          <Col fluid="true" className='dynamic-view-port'>
              <Switch>
                <Route  path='/application/welcome'>
                  <WelcomeComponent/>
                </Route>
                <Route  path='/application/create'>
                  <AddDummy />
                </Route>
                <Route  path='/application/list' >
                  <DummyList/>
                </Route>
              </Switch>         
          </Col>
        </div>
      </Col>
    </Row>
  );
}
