import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import { Container } from 'react-bootstrap';
import LoginScreen from './components/LoginComponent/LoginScreen';
import MainLayoutComponent from './components/MainLayout/MainLayoutComponent';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

function App() {
  return (
   
    <Container fluid>
       <Router>
        <Switch>
          <Redirect from='/' to='/login' exact />
          <Route exact path='/login'>
            <LoginScreen />
          </Route>
          <Route path='/application'>
          <MainLayoutComponent />
          </Route>
        </Switch>
     
      </Router>
      </Container>
  
  );
}

export default App;
